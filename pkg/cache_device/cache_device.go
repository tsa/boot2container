package cache_device

import (
	"fmt"
	"log"
	"sort"
	"strings"
	"time"

	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/cmdline"
	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/filesystem"

	"github.com/diskfs/go-diskfs/partition/gpt"
)

const (
	CACHE_PARTITION_FS    = "ext4"
	CACHE_PARTITION_LABEL = "B2C_CACHE"

	CACHE_DEVICE_MIN_SIZE_MiB = 256
	CACHE_DEVICE_ESP_SIZE_MiB = 128

	DEVICE_POLLING_DELAY   = 500 * time.Millisecond
	DEVICE_POLLING_TIMEOUT = 10 * time.Second
)

var (
	CACHE_PARTITION_FEATURES = []string{"encrypt"}

	// Allow tests to override the following functions to simplify testing
	CreateBlockDevice    = Device
	Sleep                = time.Sleep
	GetBlockDevices      = getBlockDevices
	PartitionCacheDevice = partitionCacheDevice
	FormatCacheDevice    = formatCacheDevice
)

type BlockDevType int

const (
	Disk BlockDevType = iota
	Partition
)

func (t BlockDevType) String() string {
	return [...]string{"Disk", "Partition"}[t]
}

type BlockDevLike interface {
	Name() string
	Path() string
	Type() BlockDevType
	Size() uint64

	// Only applicable when Type=Partition
	FSType() string
	FSLabel() string

	IsSuitableAsCachePartition() (bool, []string)
	SuitabilityScore() float32
	ToFilesystem(name string) FilesystemLike
	PartitionById(id int) BlockDevLike
}

type BlockDev struct {
	_name string
	_path string
	_type BlockDevType
	_size uint64
	// TODO: add the transport (NVME/USB/MMC/...)

	// Only applicable when Type=Partition
	_fsType  string
	_fsLabel string
}

func (b BlockDev) Name() string {
	return b._name
}

func (b BlockDev) Path() string {
	return b._path
}

func (b BlockDev) Type() BlockDevType {
	return b._type
}

func (b BlockDev) Size() uint64 {
	return b._size
}

func (b BlockDev) FSType() string {
	return b._fsType
}

func (b BlockDev) FSLabel() string {
	return b._fsLabel
}

func (b BlockDev) IsSuitableAsCachePartition() (suitable bool, issues []string) {
	if b._fsType != CACHE_PARTITION_FS {
		issues = append(issues, "The file system is not EXT4")
	}

	if b._size < CACHE_DEVICE_MIN_SIZE_MiB*1024*1024 {
		issues = append(issues, fmt.Sprintf("The size is under %d MiB", CACHE_DEVICE_MIN_SIZE_MiB))
	}

	suitable = (len(issues) == 0)

	return
}

func (b BlockDev) SuitabilityScore() float32 {
	score := float32(b._size)

	score_multiplier := float32(1.0)
	if strings.HasPrefix(b._path, "/dev/nvme") {
		score_multiplier = 5
	} else if strings.HasPrefix(b._path, "/dev/hd") {
		score_multiplier = 0.5
	} else if strings.HasPrefix(b._path, "/dev/mmc") {
		score_multiplier = 0.5
	}

	return score * score_multiplier
}

func (b BlockDev) ToFilesystem(name string) FilesystemLike {
	return filesystem.FilesystemConfig{Name: name, Src: b._path, Type: b._fsType}
}

// BlockDevices is a list of block devices.
type BlockDevices []BlockDevLike

func (b BlockDevices) FilterFSLabel(fsLabel string) BlockDevices {
	partitions := make(BlockDevices, 0)
	for _, device := range b {
		if device.FSLabel() == fsLabel {
			partitions = append(partitions, device)
		}
	}
	return partitions
}

func (b BlockDevices) FilterFSType(fsType string) BlockDevices {
	partitions := make(BlockDevices, 0)
	for _, device := range b {
		if device.FSType() == fsType {
			partitions = append(partitions, device)
		}
	}
	return partitions
}

func (b BlockDevices) FilterType(Type BlockDevType) BlockDevices {
	devices := make(BlockDevices, 0)
	for _, device := range b {
		if device.Type() == Type {
			devices = append(devices, device)
		}
	}
	return devices
}

func (b BlockDevices) FilterSize(minSizeMB, maxSizeMB uint64) BlockDevices {
	devices := make(BlockDevices, 0)
	for _, device := range b {
		sz := device.Size() / 1024 / 1024
		if (minSizeMB == 0 || sz >= minSizeMB) && (maxSizeMB == 0 || sz <= maxSizeMB) {
			devices = append(devices, device)
		}
	}
	return devices
}

func (b BlockDevices) SortBySuitability() {
	sort.SliceStable(b, func(i, j int) bool { return b[i].SuitabilityScore() < b[j].SuitabilityScore() })
}

func (b BlockDevices) PrintList(msg string) string {
	var s string

	if len(b) > 0 {
		s = fmt.Sprintf("Found the following %s (%d):", msg, len(b))
		for _, device := range b {
			var size float32 = float32(device.Size()) / 1000.0 / 1000.0 / 1000.0
			s += fmt.Sprintf("\n - %s (%s): size=%.2f GB, type=%s, label=%s, score=%.0f", device.Name(),
				device.Type().String(), size, device.FSType(), device.FSLabel(), device.SuitabilityScore())
		}
		s += "\n"
	} else {
		s = fmt.Sprintf("No %s found\n", msg)
	}

	fmt.Printf(s)
	return s
}

type FilesystemLike interface {
	Source() string
	Mount(mountPoint string) error
	Format(label string, features []string) error
	Fstrim(mountPoint string) error
}

type CacheDeviceConfig struct {
	RawName string
	Mode    string

	// When Mode == "filesystem"
	Filesystem FilesystemLike

	Fstrim string
}

func (cfg CacheDeviceConfig) mountFilesystem(fs FilesystemLike, mountPoint string) bool {
	if err := fs.Mount(mountPoint); err != nil {
		log.Printf("Failed to mount '%s': %s\n", fs.Source(), err.Error())
		return false
	}

	fmt.Printf("Successfully mounted %s as a cache device\n\n", fs.Source())

	switch cfg.Fstrim {
	case "never", "":
		// Nothing to do
	case "pipeline_start":
		fmt.Printf("\n# Running fstrim on %s\n", mountPoint)
		fs.Fstrim(mountPoint)
	default:
		log.Panicf("Unhandled Fstrim value '%s'", cfg.Fstrim)
	}

	return true
}

func (cfg CacheDeviceConfig) useSpecifiedBlockDevice(mountPoint string) bool {
	fs := cfg.Filesystem

	fmt.Printf("\n# Trying to use %s as a cache device\n", fs.Source())

	for i := 0; i < int(DEVICE_POLLING_TIMEOUT/DEVICE_POLLING_DELAY); i++ {
		// Try getting the device
		dev, err := CreateBlockDevice(fs.Source())
		if err == nil && dev != nil {
			fmt.Printf("\nFound the device node, checking if it can be mounted directly\n")

			// We got the device, use it as is or re-format it if missing :)
			if isSuitable, issues := dev.IsSuitableAsCachePartition(); isSuitable {
				fmt.Printf("The block device has a compatible filesystem, mount it!\n")

				return cfg.mountFilesystem(fs, mountPoint)
			} else {
				fmt.Printf("\nWARNING: The block device does not have a compatible filesystem. Reasons:\n")
				for _, reason := range issues {
					fmt.Printf(" - %s\n", reason)
				}

				fmt.Printf("\nFormating %s to make it usable as a cache device\n", fs.Source())
				if err := fs.Format(CACHE_PARTITION_LABEL, CACHE_PARTITION_FEATURES); err != nil {
					fmt.Printf("ERROR: Failed to format the cache partition")
					return false
				}

				fmt.Printf("Mounting the re-formated block device\n")
				return cfg.mountFilesystem(fs, mountPoint)
			}
		} else if i == 0 {
			secs := DEVICE_POLLING_TIMEOUT / time.Second
			fmt.Printf("\nThe device node %s is missing... waiting up to %d seconds for it to appear\n",
				fs.Source(), secs)
		}

		Sleep(DEVICE_POLLING_DELAY)
	}

	fmt.Printf("\n# The device node %s did not appear. Defaulting to 'none'\n\n", fs.Source())
	return false
}

func (cfg CacheDeviceConfig) findMostSuitableExistingCachePartition(b BlockDevices) BlockDevLike {
	fmt.Printf("\n# Trying to find a suitable and *existing* cache partition\n\n")

	cache_partitions := b.FilterFSLabel(CACHE_PARTITION_LABEL)
	cache_partitions = cache_partitions.FilterFSType(CACHE_PARTITION_FS)
	cache_partitions.SortBySuitability()

	cache_partitions.PrintList("existing cache partitions")

	if len(cache_partitions) > 0 {
		best_partition := cache_partitions[len(cache_partitions)-1]
		if best_partition.SuitabilityScore() > 0 {
			return best_partition
		}
	}

	return nil
}

func (cfg CacheDeviceConfig) findMostSuitableCacheDisk(b BlockDevices) BlockDevLike {
	disks := b.FilterType(Disk)
	disks = disks.FilterSize(CACHE_DEVICE_MIN_SIZE_MiB, 0)
	disks.SortBySuitability()

	disks.PrintList("suitable cache disks")

	if len(disks) > 0 {
		best_disk := disks[len(disks)-1]
		if best_disk.SuitabilityScore() > 0 {
			return best_disk
		}
	}

	return nil
}

func (cfg CacheDeviceConfig) resetDiskPartitionTable(b BlockDevLike) *gpt.Table {
	// Constants
	var blkSize uint64 = 512

	szMiB := b.Size() / 1024 / 1024
	if szMiB < CACHE_DEVICE_ESP_SIZE_MiB || szMiB < CACHE_DEVICE_MIN_SIZE_MiB {
		panic("The size of the cache device is too small")
	}

	var espSize uint64 = CACHE_DEVICE_ESP_SIZE_MiB * 1024 * 1024
	var espPartitionSectorStart uint64 = 2048
	var espPartitionSectorEnd uint64 = espPartitionSectorStart + (espSize / blkSize) - 1
	esp := gpt.Partition{Start: espPartitionSectorStart, End: espPartitionSectorEnd, Type: gpt.EFISystemPartition, Name: "EFI System"}

	var cachePartitionSectorStart uint64 = espPartitionSectorEnd
	var cachePartitionSectorEnd uint64 = (b.Size() / blkSize) - 1
	cache := gpt.Partition{Start: cachePartitionSectorStart, End: cachePartitionSectorEnd, Type: gpt.LinuxFilesystem, Name: CACHE_PARTITION_LABEL}

	return &gpt.Table{
		LogicalSectorSize:  512,
		PhysicalSectorSize: 512,
		ProtectiveMBR:      true,
		Partitions:         []*gpt.Partition{&esp, &cache},
	}
}

func (cfg CacheDeviceConfig) resetDiskAndMount(blockDevs BlockDevices, mountPoint string) bool {
	fmt.Printf("\n# Trying to find the most suitable cache disk\n\n")

	blk := cfg.findMostSuitableCacheDisk(blockDevs)
	if blk == nil {
		return false
	}

	fmt.Printf("\n# Re-partitioning %s as a cache drive\n", blk.Path())

	if !PartitionCacheDevice(blk, cfg.resetDiskPartitionTable(blk)) {
		return false
	}

	// Find the partition's name
	cachePart := blk.PartitionById(2)
	if cachePart == nil {
		return false
	}

	// Format the cache partition
	fmt.Printf("\n# Formating %s to use as a cache partition\n", cachePart.Path())

	blkFs, err := FormatCacheDevice(cachePart)
	if err != nil {
		fmt.Printf("ERROR: Failed to format the cache partition\n")
		return false
	}

	fmt.Printf("\n# Mounting %s as a cache partition\n", cachePart.Path())

	return cfg.mountFilesystem(blkFs, mountPoint)
}

func (cfg CacheDeviceConfig) Mount(mountPoint string) bool {
	// If the user specified the filesystem or block to use, try using it if possible
	if cfg.Mode == "none" {
		fmt.Printf("Do not use a partition cache\n")
		return false
	} else if cfg.Mode == "filesystem" {
		return cfg.mountFilesystem(cfg.Filesystem, mountPoint)
	} else if cfg.Mode == "block" {
		return cfg.useSpecifiedBlockDevice(mountPoint)
	} else if cfg.Mode != "auto" && cfg.Mode != "reset" {
		log.Panicf("Unsupported mode '%s'\n", cfg.Mode)
	}

	// We were not asked to use a filesystem directly, so we'll need to use a local block device
	blockDevs, err := GetBlockDevices()
	blockDevs.PrintList("block devices")
	if err != nil || len(blockDevs) == 0 {
		// No block devices found, nothing to do
		return false
	}

	// Try to re-use an existing cache partition
	if blk := cfg.findMostSuitableExistingCachePartition(blockDevs); blk != nil {
		fs := blk.ToFilesystem("Cache Device")

		if cfg.Mode == "reset" {
			fmt.Printf("\n# Re-format the %s cache partition\n", fs.Source())
			fs.Format(CACHE_PARTITION_LABEL, CACHE_PARTITION_FEATURES)
		}

		fmt.Printf("\n# Mounting %s as a cache partition\n", fs.Source())
		return cfg.mountFilesystem(fs, mountPoint)
	}

	// We failed to find any pre-existing suitable cache partition, time to create one!
	return cfg.resetDiskAndMount(blockDevs, mountPoint)
}

func ParseCmdline(opt *cmdline.Option, filesystems map[string]filesystem.FilesystemConfig) CacheDeviceConfig {
	var val *cmdline.OptionValue
	if opt == nil {
		val = nil
	} else {
		val = opt.Value()
	}

	if val == nil || len(val.Args) == 0 {
		return CacheDeviceConfig{RawName: "", Mode: "none", Fstrim: "never"}
	} else if len(val.Args) > 1 {
		fmt.Printf("WARNING: The following `b2c.cache_device` arguments are unsupported: %q\n", val.Args[1:])
	}

	cfg := CacheDeviceConfig{RawName: val.Args[0], Mode: "none", Fstrim: "never"}

	// Parse the name. It can be a keyword (none, auto), a block device, or a filesystem
	if cfg.RawName == "none" || cfg.RawName == "auto" {
		cfg.Mode = cfg.RawName
	} else if strings.HasPrefix(cfg.RawName, "/dev/") {
		cfg.Mode = "block"
		cfg.Filesystem = filesystem.FilesystemConfig{Name: "CacheDevice", Src: cfg.RawName, Type: CACHE_PARTITION_FS}
	} else if fs, ok := filesystems[cfg.RawName]; ok {
		cfg.Mode = "filesystem"
		cfg.Filesystem = fs
	} else {
		fmt.Printf("WARNING: The b2c.cache_device value '%s' is unknown, defaulting to 'none'\n",
			cfg.RawName)
	}

	fstrim := val.KwArgs["fstrim"]
	if len(fstrim) > 0 {
		switch fstrim[0] {
		case "never", "pipeline_start":
			cfg.Fstrim = fstrim[0]
		default:
			fmt.Printf("WARNING: Value '%s' is unknown for fstrim, defaulting to 'never'\n", fstrim[0])
			cfg.Fstrim = "never"
		}
	}

	return cfg
}
