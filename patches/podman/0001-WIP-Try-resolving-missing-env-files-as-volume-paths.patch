From ac25c7c89903b96cc1503601e4dd08372b567dae Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Martin=20Roukala=20=28n=C3=A9=20Peres=29?=
 <martin.roukala@mupuf.org>
Date: Mon, 15 Aug 2022 17:45:58 +0300
Subject: [PATCH] WIP: Try resolving missing env files as volume paths
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

This enables sharing env files using podman volumes, rather than having
to first download the file on the host, thus making it easier to use
environment files.

TODO: Implement this for all podman commands, not just `create`

Signed-off-by: Martin Roukala (né Peres) <martin.roukala@mupuf.org>
---
 cmd/podman/containers/create.go | 44 +++++++++++++++++++++++++++++++++
 1 file changed, 44 insertions(+)

diff --git a/cmd/podman/containers/create.go b/cmd/podman/containers/create.go
index 455127fd7..30a7a1d9d 100644
--- a/cmd/podman/containers/create.go
+++ b/cmd/podman/containers/create.go
@@ -147,6 +147,50 @@ func create(cmd *cobra.Command, args []string) error {
 		}
 		imageName = name
 	}
+
+	// Try to resolve env-file paths as volume paths if they do not exist
+	for i, path := range cliVals.EnvFile {
+		if _, err := os.Stat(path); errors.Is(err, os.ErrNotExist) {
+			logrus.Warnf("The env-file '%s' cannot be opened, trying to resolve the path in a volume", path)
+
+			// Ignore absolute paths, as they can't possibly reference a volume
+			if strings.HasPrefix(path, "/") {
+				logrus.Debugf("  The path '%s' starts with /, so it cannot be from a volume. Ignoring!", path)
+				continue
+			}
+
+			// Ignore paths that would only resolve to a volume name
+			path_fields := strings.Split(path, "/")
+			if len(path_fields) == 1 {
+				logrus.Debugf("  The path '%s' can be nothing but a volume name. Ignoring!", path)
+				continue
+			}
+
+			// Try to resolve the first "directory" of the path as a volume name
+			volume_name := path_fields[0]
+			inspectOpts.Type = common.VolumeType
+			volumes := []string {volume_name}
+			volumeData, errs, err := registry.ContainerEngine().VolumeInspect(registry.GetContext(), volumes, *inspectOpts)
+			if err != nil || len(errs) > 0 || len(volumeData) < 1 {
+				logrus.Debugf("  The volume_name '%s' can't be inspected (errs=%q). Ignoring!", path, errs)
+				continue
+			}
+
+			// We found a matching volume, create the candidate new path for the file!
+			mountPoint := volumeData[0].VolumeConfigResponse.Mountpoint
+			path_fields[0] = mountPoint
+			new_path := strings.Join(path_fields, "/")
+
+			// If the candidate path exists, replace the original path!
+			if _, err := os.Stat(new_path); err == nil {
+				cliVals.EnvFile[i] = new_path
+				logrus.Infof("  Using the alternative path '%s'!", new_path)
+			} else {
+				logrus.Warnf("  The alternative path '%s' does not exist. Ignoring!", new_path)
+			}
+		}
+	}
+
 	s := specgen.NewSpecGenerator(imageName, cliVals.RootFS)
 	if err := specgenutil.FillOutSpecGen(s, &cliVals, args); err != nil {
 		return err
-- 
2.37.2

